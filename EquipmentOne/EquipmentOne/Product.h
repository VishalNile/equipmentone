//
//  Product.h
//  EquipmentOne
//
//  Created by Vishal Nile on 19/10/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property(nonatomic,retain)NSString *strProductName,*strBid,*location,*strTime;
@property(nonatomic,retain)NSString *strImgUrl;
@end
