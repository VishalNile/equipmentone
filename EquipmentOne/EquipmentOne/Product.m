//
//  Product.m
//  EquipmentOne
//
//  Created by Vishal Nile on 19/10/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import "Product.h"

@implementation Product
@synthesize strProductName,strBid,location,strTime;
@synthesize strImgUrl;

-(void)dealloc
{
    strProductName=nil;
    strBid=nil;
    location=nil;
    strTime=nil;
    strImgUrl=nil;
}
@end
