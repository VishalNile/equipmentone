//
//  LoginViewController.h
//  ePOd
//
//  Created by ITIM  on 02/09/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viwContainer;
@property (strong, nonatomic) IBOutlet UITextField *txtUser;
@property (strong, nonatomic) IBOutlet UITextField *txtPass;
@property (weak, nonatomic) IBOutlet UIView *viwDialogue;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@end
