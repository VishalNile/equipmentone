//
//  LoginViewController.m
//  ePOd
//
//  Created by ITIM  on 02/09/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viwDialogue.layer.borderColor=[UIColor whiteColor].CGColor;
    self.viwDialogue.layer.borderWidth=1;
    self.viwDialogue.layer.cornerRadius=5;
    
    self.btnLogin.layer.cornerRadius=5;

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
    if (self.txtUser.text.length==0  && self.txtPass.text.length==0)
    {
        // perform your computation to determine whether segue should occur
        
      
            UIAlertView *alt=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter user name and password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alt show];
            alt=nil;
        return NO;
    }
    
    // by default perform the segue transition
    [self keyboardWillHide];
    return YES;
}


#pragma mark textField delgate methods
-(void)keyboardWillHide
{
    if ([self.txtUser isFirstResponder])
    {
       [self.txtUser resignFirstResponder];
    }else if ([self.txtPass isFirstResponder])
    {
        [self.txtPass resignFirstResponder];
    }
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = -130;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"aimation" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame= CGRectOffset(self.view.frame,0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
