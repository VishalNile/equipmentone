//
//  HomeViewController.h
//  EquipmentOne
//
//  Created by Vishal Nile on 16/10/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAL.h"

@interface HomeViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray *arrProduct;
    DAL *objDal;
}
@property (weak, nonatomic) IBOutlet UIView *viwLeft;
@property (weak, nonatomic) IBOutlet UIView *viwTop;
@property (weak, nonatomic) IBOutlet UIView *viwContainer;
@property (weak, nonatomic) IBOutlet UIView *viwBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
-(IBAction)btnBackClicked:(id)sender;
@end
