//
//  HomeViewController.m
//  EquipmentOne
//
//  Created by Vishal Nile on 16/10/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viwBottom.layer.borderWidth=1;
    self.viwBottom.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    self.viwTop.layer.borderWidth=1;
    self.viwTop.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.colView.delegate=self;
    objDal=[[DAL alloc]init];
    arrProduct=[objDal getProduct];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark CollectionView delagate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrProduct.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    UICollectionViewCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"colIdentifier" forIndexPath:indexPath];
    cell.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.layer.borderWidth=0.5;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
