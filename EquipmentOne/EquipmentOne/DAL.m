//
//  DAL.m
//  EquipmentOne
//
//  Created by Vishal Nile on 19/10/15.
//  Copyright (c) 2015 Quinnox. All rights reserved.
//

#import "DAL.h"
#import "Product.h"

@implementation DAL

-(NSArray *)getProduct
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"dummy_data"
                                                     ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSError *error;
    NSArray *arrData = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    // Parsing is pending
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int i=0; i<arrData.count; i++)
    {
        Product *objProd=[[Product alloc]init];
        NSDictionary *dicPar=[arrData objectAtIndex:i];
        objProd.strProductName=[dicPar valueForKey:@"title"];
        objProd.strBid=@"22,000,00";
        objProd.strImgUrl=[dicPar valueForKey:@"image"];
        objProd.strTime=[dicPar valueForKey:@"endTimestamp"];
        [arrTemp addObject:objProd];
        objProd=nil;
        dicPar=nil;
    }
    
    
    return arrTemp;
}
@end
